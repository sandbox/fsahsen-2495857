# fsahsen/drupal-nfauth


# Single Sign On (Basic).
Allows users with netforum profiles to sign in into the Drupal website.
This provides basic SSO functionality and will create new users in Drupal if they don’t exist.
In addition, this also sets certain user attributes using the netForum SSO Tokens, that will let you create cross-site hyperlinks,
allowing users to seamlessly move between the Drupal CMS and Netforum eWeb site.


# Requirements
* This package requires PHP 5.4+
* [Git](https://git-scm.com/) installed.
* [Composer](https://getcomposer.org/) installed.
* [Composer Manager](https://www.drupal.org/project/composer_manager) Drupal Module


# Installation
* Download the latest [netforum drupal plugin](https://bitbucket.org/fsahsen/drupal-nfauth).

* Install [Composer Manager](https://www.drupal.org/project/composer_manager) plugin for drupal.
Once installed, replace the contents of the `composer.json` file located at `sites/default/files/composer/composer.json` with the following.

```php
{
"require": {
"php": ">=5.3.0"
},
"config": {
"autoloader-suffix": "ComposerManager",
"vendor-dir": "../../../all/libraries/composer"
},
"repositories": [
{
"type": "vcs",
"url" : "https://bitbucket.org/fsahsen/netforum-basic.git"
}
],
"require": {
"fsahsen/netforum-basic": "~0.1@dev"
}
}
```

* Update your dependency with `composer update` (You will need ssh access to run composer update command).


# Contribution Guidelines

Support follows PSR-2 PHP coding standards, and semantic versioning.

Please report any issue you find in the issues page.
Pull requests are welcome.