<?php

/**
 * @file
 * Helper functions to return a users uid based on their username.
 *
 * @param $username
 * @return
 */


function _netauth_get_uid_from_username($username) {
  return db_query('SELECT uid from {users} WHERE name = :name', [
    ':name' => $username
  ])->fetchField();
}

/**
 * @param $username
 * @return mixed
 */
function _netauth_get_external_uid_from_username($username) {
  return db_query('SELECT uid FROM {authmap} WHERE authname = :authname AND module = :module', [
    ':authname' => $username,
    ':module'   => 'netauth',
  ])->fetchField();
}


/**
 * Gets the drupal uid whose netFORUM customer key is within the database
 * or returns false if that key is not stored in the database
 *
 * @param $cst_key
 * @return mixed
 */
function _netauth_get_uid_from_cst_key($cst_key) {
  return db_query('SELECT user_id FROM {fusionspan_netauth} WHERE cst_key = :cst_key', [
    ':cst_key' => $cst_key,
  ])->fetchField();
}

function _netauth_get_cst_key_from_uid($uid) {
  return db_query('SELECT cst_key FROM {fusionspan_netauth} WHERE user_id = :user_id', [
    ':user_id' => $uid,
  ])->fetchField();
}

function _netauth_set_cst_key_uid($uid, $cst_key, $member_flg = 0) {
  return db_insert('fusionspan_netauth')->fields([
    'user_id'     => $uid,
    'cst_key'     => $cst_key,
    'member_flag' => $member_flg
  ])->execute();
}

function _netauth_update_authmap_authname($name, $uid) {
  return db_update('authmap')->fields([
    'authname' => $name
  ])
    ->condition('uid', $uid)
    ->execute();
}

/**
 * Gets the member_flag from the netauth_fs_netauth table, either based on the
 * customer key, or the uid
 *
 * @param string $cst_key
 * @param string $uid
 * @return false if record does not exsist
 */
function _netauth_get_member_flag($cst_key = NULL, $uid = NULL) {
  if ($uid == NULL && $cst_key == NULL) {
    return FALSE;
  }
  if ($cst_key == NULL) {
    return db_query('SELECT member_flag FROM {fusionspan_netauth} WHERE user_id = :user_id', [
      ':user_id' => $uid,
    ])->fetchField();
  }
  else {
    return db_query('SELECT member_flag FROM {fusionspan_netauth} WHERE cst_key = :cst_key', [
      ':cst_key' => $cst_key,
    ])->fetchField();
  }


}

/**
 * Updates the table specified by $tbl_name
 *
 * @param unknown $tbl_name - table name
 * @param unknown $field_array - array with the field name and values to update
 * @param unknown $condition - a size one array where the key is the field name
 *   and the value is the condition value
 */
function _netauth_db_update($tbl_name, $field_array, $condition) {
  reset($condition);
  $key = key($condition);
  db_update($tbl_name)->fields($field_array)
    ->condition($key, $condition[$key])
    ->execute();
}

/**
 * @param $name
 * @return mixed
 */
function _netauth_get_role_by_name($name) {
  return array_search($name, user_roles());
}
