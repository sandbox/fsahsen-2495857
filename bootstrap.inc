<?php
/**
 * @file
 * Bootstrap function for the module
 */

if (!function_exists('find_files')) {
  /**
   * @param       $directory
   * @param array $extensions
   * @return array
   */
  function find_files($directory, $extensions = []) {
    $directories = [];
    if (!function_exists('glob_recursive')) {
      /**
       * @param       $directory
       * @param array $directories
       */
      function glob_recursive($directory, &$directories = []) {
        foreach (glob($directory, GLOB_ONLYDIR | GLOB_NOSORT) as $folder) {
          $directories[] = $folder;
          glob_recursive("{$folder}/*", $directories);
        }
      }
    }
    glob_recursive($directory, $directories);
    $files = [];
    foreach ($directories as $directory) {
      foreach ($extensions as $extension) {
        foreach (glob("{$directory}/*.{$extension}") as $file) {
          $files[$extension][] = $file;
        }
      }
    }

    return $files;
  }
}

@array_walk_recursive(find_files(__DIR__, ['php', 'inc', 'module']),
  /**
   * @param $f
   */
  function ($f) {
    require_once $f;
  });